<?php
    require_once 'animal.php';
    require_once 'frog.php';
    require_once 'ape.php';

    $sheep = new Animal('shaun');

    // $sheep->set_name("shaun");

    echo 'Animal name: '.$sheep->get_name().'<br>';
    echo 'Legs: '.$sheep->legs.'<br>';
    echo 'Cold blooded: '.$sheep->cold_blooded.'<br><br>';

    $kodok = new Frog('buduk');
    echo 'Frog name: '.$kodok->get_name().'<br>';
    echo 'Legs: '.$kodok->legs.'<br>';
    echo 'Cold blooded: '.$kodok->cold_blooded.'<br>';
    echo $kodok->jump().'<br><br>';


    $sungokong = new Ape('kera sakti');
    echo 'Ape name: '.$sungokong->get_name().'<br>';
    echo 'Legs: '.$sungokong->legs.'<br>';
    echo 'Cold blooded: '.$sungokong->cold_blooded.'<br>';
    echo $sungokong->yell().'<br>';
?>