<?php
    class Animal{

        public $name;
        public $legs = 2;
        public $cold_blooded = "false";

        public function __construct($name1){
            $this->name = $name1;
        }

        // public function set_name($name1){
        //     $this->name = $name1;
        // }

        public function get_name(){
            return $this->name;
        }
    }
?>